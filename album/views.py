from django.shortcuts import render,redirect
from django.http import HttpResponse
from album.models import Category,Photo
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.views.generic import ListView,DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm

@login_required
def base(request):
	context = {'image_count': len(Photo.objects.all())}
	return render(request,'base.html',context)

def first_view(request):
	return HttpResponse("My first view!");

@login_required
def category(request):
	category_list = Category.objects.all()
	context = {'object_list': category_list}
	return render(request,'album/category.html',context)

@login_required
def category_detail(request,category_id):
	category = Category.objects.get(id=category_id)
	context = {'object':category}
	return render(request,'album/category_detail.html',context)

@login_required
def user_options(request):
	return render(request,'album/user_options.html')

def create_user(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/album/')
			# context = {'image_count': len(Photo.objects.all())}
			# return render(request, 'base.html', context)
	else:
		form = UserCreationForm()
	context = {"form":form}
	return render(request,'registration/user_creation.html',context)

class PhotoListView(LoginRequiredMixin,ListView):
	model = Photo

class PhotoDetailView(LoginRequiredMixin,DetailView):
	model = Photo


class PhotoUpdate(LoginRequiredMixin,UpdateView):
	model = Photo
	fields = '__all__'

class PhotoCreate(LoginRequiredMixin,CreateView):
	model = Photo
	fields = '__all__'


class PhotoDelete(LoginRequiredMixin,DeleteView):
	model = Photo
	success_url = reverse_lazy('photo-list')
		

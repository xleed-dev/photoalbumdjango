# PhotoAlbumDjango

Photo Album made with Django and Boostrap

# Screenshots:
## AdminLTE UI
### Main Page
![adminlte_base](https://gitlab.com/uploads/-/system/temp/5f079983ff2b2f5b4c4dc90525280de5/adminlte_base.png)
### Photo List
![adminlte_photos](https://gitlab.com/uploads/-/system/temp/91b45661701f244a7f8f325b47fb87cb/adminlte_photos.png)
### Categories
![adminlte_categories](https://gitlab.com/uploads/-/system/temp/3423564c8c1b7e543f8052a4a3d24bd0/adminlte_categories.png)
### Photo Upload
![adminlte_upload](https://gitlab.com/uploads/-/system/temp/718531aadcea945b1a4a9f1bcbefd62a/adminlte_upload.png)
### User Options
![adminlte_user_options](https://gitlab.com/uploads/-/system/temp/c2df2ced9a9e64568ed8c3223ce3e7fd/adminlte_user_options.png)

## Clasic UI
#### Photo List
![photo_list](https://gitlab.com/uploads/-/system/temp/5a0f8052e4b67a59d1becea51e13b315/photo_list.PNG)
#### Album Photo List
![album_photo_list](https://gitlab.com/uploads/-/system/temp/390479506706216a9a3c498be4652af4/album_photo_list.PNG)
#### Album Categories
![categories](https://gitlab.com/uploads/-/system/temp/6c4c8a7ce6ef3a11ba8623531995fabb/categories.PNG)